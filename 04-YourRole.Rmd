# Your Role

:::: {.notebox data-latex=""}
::: {.center data-latex=""}
**Course Note:**
:::

This chapter is still under construction. 
::::

Research main task is to interrogate nature to obtain an answer to a question. Most of the work is put on translating the question into methods (e.g., a lab experiment, a data analysis) that yield an answer. However, ensuring that the answer we obtained is the answer to the question we had requires a complete and unpolluted understanding of the research workflow. After all, we never test an hypothesis alone, we may take wrong assumptions, we may chose the wrong methods, and in turn, our answer may be confounded or mixed up with the effects of our errors. Within our specific roles, there is much we can do to avoid these mistakes from propagating or finding them just too late.

## What can I do as a PI?

Provide the necessary resources and requirements to your team. **Develop a plan to audit the models** and set goals other than the conventional technical metrics of precision, accuracy, etc. **Create a space for open debate**, encourage your team to talk about their doubts, questions and problems. Otherwise, we risk them always presenting how awesome is their work, blinding the possibility of spotting wrong assumptions and methodological issues.

## What can I do as a postdoc?

**Do not wait for your PI to audit your work**, be pro-active. A proper understanding of the problem at issue is key, then exploratory data analysis is crucial to spot weaknesses (e.g., data may be partially representative). Play devil's advocate. Since data is just a blurry shadow of reality, with an inherent margin of error with respect to reality, you must think, how is this data wrong? can I quantify this error? even if my data is not perfect, can it be useful to answer my research question? what are their limitations? 

Finally, discuss! present your work to your colleagues, ask questions, showcase your doubts and fears.

## What can I do as a MsC, PhD student?

Be completely open with your colleagues about your doubts, misunderstandings and questions. Only this way you will learn and develop a critical eye. Do not fall into conventionalisms, justify why the methods you employ are better (or more adequate) than others (e.g., why ML instead of conventional statistics?, why use neural networks instead of a decision tree?).

