--- 
title: "Artificial Intelligence Guidelines for the LCSB"
author: ["Dr. Carlos Vega"]
date: "2022 - 2023: Last compiled: `r Sys.Date()`"
site: bookdown::bookdown_site
output: 
  bookdown::gitbook:
    includes:
      in_header: includeme.html
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
github-repo: carlosvega/PoS-DE
description: "This book is a draft of a Policy and/or Guideline for the development of Artificial Intelligence solutions at the Luxembourg Centre for Systems Biomedicine."
urlcolor: blue
linkcolor: red
---

# Preface {-}


## Disclaimer {-}


## Outcome Summary {-}

Artificial Intelligence is meant to solve problems through automation without creating further issues in the way. For certain tasks like medical diagnosis and complex decision making, training specialised people (e.g., doctors, nurses, etc.) is the best way to achieve long-term stability and performance. If even then, we decide to build AI models, we must bear in mind several kind of issues that may arise.

- Issues derived from employing wrong data to train our models. 
  - e.g., unrepresentative data.
- Issues derived from wrong labelling of our data. 
  - e.g., lack of a proper taxonomy or availability of noisy labels.
- Issues derived from unrepresentative performance metrics that give an appearance of performance.
  - Plain accuracy and precision values, like 99% precision, do not give enough granularity regarding the performance of our model. We should use other metrics or stratify the performance assessment by subgroups.
- Issues derived from unplanned maintenance of the models. Like us, models need constant re-training!
  - e.g., data drift and concept drift.
- Issues derived from poor design decisions.
  - e.g., the model we chose may not be able to learn a function that is faifthful with respect to the phenomena we intend to model.

<!-- - Get familiar with ... -->
<!-- - Learn the most common ... -->
<!-- - ... -->

## About this book {-}


### License {-}

**Licensed under CC BY-NC-ND 4.0**

The book is released under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license. This means that you are free to: 

- **Share**: copy and redistribute the material in any medium or format.

Under the following terms:

- **Attribution**: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- **Non-Commercial**: You may not use the material for commercial purposes.
- **No Derivatives**: If you remix, transform, or build upon the material, you may not distribute the modified material.
- **No additional restrictions**: You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## About the authors {-}

### Carlos Vega {-}

Since September 2018, [Carlos Vega](https://wwwen.uni.lu/lcsb/people/carlos_vega_moreno) works as a postdoctoral researcher in the Bioinformatics Core Group led by Prof. Dr Reinhard Schneider at the Luxembourg Centre for Systems Biomedicine (L.C.S.B) at the University of Luxembourg. 

Previously, he worked at the Autonomous University of Madrid (UAM) researching high-performance solutions for big data analysis as well as anomaly detection methodologies. He received his B.Sc (2013), M.Sc (2014) and PhD (2018 with *cum laude* and industrial mentions) degrees in Computer Science Engineering from UAM. In 2012 he joined the High-Performance Computing and Networking Research Group (HPCN) led by Prof. Dr Javier Aracil, first as a student and later as a researcher as part of the Network of Excellence InterNet Science European project. During his PhD (2014 - 2017), he continued his work at the HPCN group as a technical researcher for the project TRÁFICA and the European projects Fed4Fire and dReDBox, among others. At the same time, he worked at [Naudit](https://www.naudit.es/en) (2015 - 2018) applying his research in computer network auditing projects with different enterprises. In 2022, he became a senior member of the Institute of Electrical and Electronics Engineers (IEEE).

Additionally, his teaching experience includes several courses on computer networks, such as Multimedia Networking, Network Planning and Network Management, taught during his time at UAM. Since 2021, he is the main teacher of the course "Applied Philosophy of Science and Data Ethics" for the Master of Data Science at the Faculty of Science, Technology and Medicine (FSTM) of the University of Luxembourg.

### Contributing to these guidelines {-}

...
